import Sums from 'components/sums';
import Table from 'components/table';
import tableData from 'data/data.json';
import React, { createContext, useState } from 'react';
import './App.css';



export const AppContext = createContext()

function App() {
	const [data, setData] = useState(tableData)

	const defaultContext = {
		data,
		setData
	}

	return (
		<main>
			<AppContext.Provider value={defaultContext}>
				<h1>Le Table:</h1>
				<Table />
				<br />

				<h1>Le Sums:</h1>
				<Sums />
			</AppContext.Provider>
		</main>
	)
}

export default App;
