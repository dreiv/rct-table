import React from 'react';
import './TableFilter.css';

export default props => (
	<label className="filter">
		Filter:{' '}
		<input
			type="search"
			{...props}
		/>
	</label>
)
