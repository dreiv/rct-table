import { AppContext } from 'components/app/App';
import React, { useContext } from 'react';
import { useSort } from '../hooks';
import './TableHeader.css';


const SortSymbol = ({ order }) => (
	{
		'desc': <span className='sort'>⇓</span>,
		'asc': <span className='sort'>⇑</span>
	}[order] || <span className='sort sort-default'>⇕</span>
)

export default () => {
	const { data: { columns } } = useContext(AppContext)
	const { handleSort } = useSort()

	return (
		<thead>
			<tr>
				{columns.map(({ name, order }, idx) => (
					<th key={idx} onClick={handleSort(idx)}>
						{name}
						<SortSymbol order={order}/>
					</th>
				))
				}
			</tr>
		</thead>
	)
}
