import { AppContext } from 'components/app/App';
import { useContext } from 'react';

export default () => {
	const {
		data,
		data: { rows },
		setData
	} = useContext(AppContext)

	const handleChange = (index, prop) =>
		({ target: { value } }) => {
			const newRows = [...rows]
			newRows[index][prop] = value

			setData({ ...data, rows: newRows })
		}

	return {
		handleChange
	}
}
