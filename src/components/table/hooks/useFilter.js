import { useState } from 'react';

export default () => {
	const [search, setSearch] = useState('');

	const handleSearchChange = ({ target: { value } }) => {
		setSearch(value)
	}
	const filterRow = row =>
		!search || row.name.toLowerCase().includes(search.toLowerCase())

	return {
		search,
		filterRow,
		handleSearchChange
	}
}
