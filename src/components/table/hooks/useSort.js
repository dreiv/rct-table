import { AppContext } from 'components/app/App';
import { useContext } from 'react';

const comparator = (prop, desc = true) => (a, b) => {
	const order = desc ? -1 : 1

	if (a[prop] < b[prop]) {
		return -1 * order
	}

	if (a[prop] > b[prop]) {
		return 1 * order
	}

	return 0 * order
}

export default () => {
	const {
		data: { columns, rows },
		setData
	} = useContext(AppContext)

	const handleSort = index => () => {
		let newColumns = [...columns]
		newColumns = newColumns.map((col, i) => ({
			...col,
			order:
				(index === i &&
					(col.order === 'desc' ? 'asc' : 'desc')) ||
					undefined
		}))

		const newRows = [...rows]
		newRows.sort(
			comparator(
				columns[index].name.toLowerCase(),
				columns[index].order === 'desc'
			)
		)

		setData({
			columns: newColumns,
			rows: newRows
		})
	}

	return { handleSort }
}
