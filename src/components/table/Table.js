import { AppContext } from 'components/app/App';
import React, { useContext } from 'react';
import { useChange, useFilter } from './hooks';
import TableFilter from './table-filter';
import TableHeader from './table-header';
import './Table.css';

export default () => {
	const { data: { columns, rows } } = useContext(AppContext)
	const { search, filterRow, handleSearchChange } = useFilter()
	const { handleChange } = useChange()

	const renderRow = ({ id, ...row }, idx) => (
		<tr key={id}>
			{Object.entries(row).map(renderItem(idx))}
		</tr>
	)

	const renderItem = idx => ([key, value], index) => (
		<td key={key}>
			<input
				type={columns[index].type}
				value={value}
				onChange={handleChange(idx, key)}
			/>
		</td>
	)

	return (
		<>
			<TableFilter
				value={search}
				onChange={handleSearchChange}
			/>

			<table>
				<TableHeader />

				<tbody>
					{rows
						.filter(filterRow)
						.map(renderRow)
					}
				</tbody>
			</table>
		</>
	)
}
