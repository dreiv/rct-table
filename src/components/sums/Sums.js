import { AppContext } from 'components/app/App';
import React, { useContext } from 'react';

export default () => {
	const { data: { rows } } = useContext(AppContext)

	const sums = rows.reduce(
		(acc, row) => [
			acc[0] + Number(row.high),
			acc[1] + Number(row.low),
			acc[2] + Number(row.average)
		],
		[0, 0, 0]
	)

	return (
		<>
			<p>Sum High: {sums[0]}</p>
			<p>Sum Low: {sums[1]}</p>
			<p>Sum Average: {sums[2]}</p>
		</>
	)
}
